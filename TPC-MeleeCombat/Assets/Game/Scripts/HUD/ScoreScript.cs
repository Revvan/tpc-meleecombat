﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreScript : MonoBehaviour
{
    #region Singleton

    public static ScoreScript instance;

    private void Awake()
    {
        instance = this;
    }
    #endregion;

    public int currentScore, enemyPoints;

    public Text scoreText;


    void Update()
    {
        scoreText.text = "Score: " + currentScore.ToString();
    }
}
