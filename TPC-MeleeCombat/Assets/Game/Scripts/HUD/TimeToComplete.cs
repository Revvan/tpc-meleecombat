﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeToComplete : MonoBehaviour
{
    #region Singleton

    public static TimeToComplete instance;

    private void Awake()
    {
        instance = this;
    }

    #endregion

    [Tooltip("Initial time in seconds")]
    public int initialTime;

    [Tooltip("Clock time scale")]
    [Range(-10.0f, 10.0f)]
    public float scaleOfTime = 1f;

    private Text myText;
    public float frameTimeScale = 0f, showTimeInSeconds = 0f, pauseScaleTime, beginScaleTime;
    public bool inPause = false;

    public GameObject spawn, spawn2, spawn3;
    void Start()
    {
        myText = GetComponent<Text>();

        beginScaleTime = scaleOfTime;

        showTimeInSeconds = initialTime;

        UpdateClock(initialTime);


        spawn.SetActive(false);
        spawn2.SetActive(false);
        spawn3.SetActive(false);
    }

    void Update()
    {
        if (!inPause)
        {
            frameTimeScale = Time.deltaTime * scaleOfTime;

            showTimeInSeconds += frameTimeScale;
            UpdateClock(showTimeInSeconds);
        }


        if (showTimeInSeconds >= 60f)
        {
            spawn.SetActive(true);
        }
        if (showTimeInSeconds >= 120f)
        {
            spawn2.SetActive(true);
        }
        if (showTimeInSeconds >= 180f)
        {
            spawn3.SetActive(true);
        }
    }

    public void UpdateClock(float timeInSeconds)
    {
        int minutes = 0;
        int seconds = 0;
        string clockText;

        if (timeInSeconds < 0)
            timeInSeconds = 0;

        minutes = (int)timeInSeconds / 60;
        seconds = (int)timeInSeconds % 60;

        clockText = minutes.ToString("00") + ":" + seconds.ToString("00");

        myText.text = clockText;
    }

    public void Pause()
    {
        if (!inPause)
        {
            inPause = true;
            pauseScaleTime = scaleOfTime;
            scaleOfTime = 0;
        }
    }
    public void Continue()
    {
        if (inPause)
        {
            inPause = false;
            scaleOfTime = pauseScaleTime;
        }
    }
    public void ResetTimer()
    {
        inPause = false;
        scaleOfTime = beginScaleTime;
        showTimeInSeconds = initialTime;
        UpdateClock(showTimeInSeconds);
    }
}
