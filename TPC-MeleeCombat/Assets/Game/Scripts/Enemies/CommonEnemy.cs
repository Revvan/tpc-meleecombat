﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Animations;


[RequireComponent(typeof(NavMeshAgent))]
public class CommonEnemy : MonoBehaviour
{
    #region Singleton

    public static CommonEnemy instance;

    private void Awake()
    {
        instance = this;
    }

    #endregion

    #region References
    public Animator anim;
    public NavMeshAgent agent;
    public Transform targetPlayer;
    #endregion

    #region GizmoRange
    public float meleeRange;
    #endregion


    #region Stats
    public int maxHP, damage, giveExperience;
    private int currentHP;
    public BoxCollider weaponCollider;
    public EnemyHealthBar healthBar;
    #endregion

    private float playerDistance;
    void Start()
    {
        #region SetReferences
        anim = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        targetPlayer = GameObject.FindGameObjectWithTag("Player").transform;
        #endregion

        currentHP = maxHP;
        healthBar.SetMaxHealth(maxHP);
    }
    void Update()
    {
        #region AreaDetection
        float distance = Vector3.Distance(this.transform.position, targetPlayer.position);

        agent.SetDestination(targetPlayer.position);
        anim.SetBool("run", true);

        if (distance <= meleeRange)
        {
            AutoLooker();
            anim.SetBool("run", false);
            anim.SetBool("attack", true);
            agent.stoppingDistance = 2.5f;
        }
        else
        {
            agent.stoppingDistance = 0f;
        }
        #endregion


        #region ColliderOfAttackAnimation
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("Run"))
        {
            weaponCollider.enabled = false;
        }
        #endregion


        #region HealthBar&Destroyer
        healthBar.SetHealth(currentHP);
        if (currentHP <= 0)
        {
            ScoreScript.instance.currentScore += ScoreScript.instance.enemyPoints;
            PlayerController.instance.currentExp += giveExperience;
            Destroy(this.gameObject);
        }
        #endregion

        SlowArea();
    }

    #region Active/DeactivateCollider
    public void ActiveCollider()
    {
        weaponCollider.enabled = true;
    }
    public void DesactivateCollider()
    {
        weaponCollider.enabled = false;
    }
    #endregion

    public void SlowArea()
    {
        playerDistance = Vector3.Distance(targetPlayer.position, this.transform.position);

        if (AllAbilities.instance.activeArea && playerDistance < AllAbilities.instance.areaSlow)
        {
            anim.speed = AllAbilities.instance.slow;
            agent.speed = 1f;
        }
        else
        {
            anim.speed = 1f;
            agent.speed = 3.5f;
        }
    }

    public void AutoLooker()
    {
        Vector3 direccion = (targetPlayer.position - transform.position).normalized;
        Quaternion mirar = Quaternion.LookRotation(new Vector3(direccion.x, direccion.y, direccion.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, mirar, Time.deltaTime);
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(this.transform.position, meleeRange);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "PlayerSword")
        {
            currentHP -= PlayerController.instance.damage;
        }
    }
}
