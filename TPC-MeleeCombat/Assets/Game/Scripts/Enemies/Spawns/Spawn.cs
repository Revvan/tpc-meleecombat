﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour
{
    public Transform spawnPos;
    public GameObject enemyToSpawn;
    public float timeBetweenSpawns, currentTime;

    private void Start()
    {
        currentTime = timeBetweenSpawns;
    }
    void Update()
    {
        currentTime -= Time.deltaTime;

        SpawnEnemies();
    }

    private void SpawnEnemies()
    {
        if (currentTime <= 0)
        {
            Instantiate(enemyToSpawn, spawnPos.position, spawnPos.rotation);
            currentTime = timeBetweenSpawns;
        }
    }
}
