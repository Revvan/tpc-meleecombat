﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuInGameScript : MonoBehaviour
{
    #region Singleton

    public static MenuInGameScript instance;

    private void Awake()
    {
        instance = this;
    }
    #endregion;

    public GameObject menuHolder;
    public GameObject buttonContinue, buttonRestart, buttonBackToMainMenu;


    public bool pauseActive;
    void Start()
    {
        Time.timeScale = 1;

        menuHolder.SetActive(false);
        buttonContinue.SetActive(false);
        buttonRestart.SetActive(false);
        buttonBackToMainMenu.SetActive(false);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            PauseGame();
            TimeToComplete.instance.Pause();
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            AbilitiesSystem.instance.inAB = false;
            menuHolder.SetActive(true);
            buttonContinue.SetActive(true);
            buttonRestart.SetActive(true);
            buttonBackToMainMenu.SetActive(true);
        }
        else if(pauseActive == false && !AbilitiesSystem.instance.inAB)
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
            TimeToComplete.instance.Continue();

            menuHolder.SetActive(false);
            buttonContinue.SetActive(false);
            buttonRestart.SetActive(false);
            buttonBackToMainMenu.SetActive(false);
        }
    }
    public void PauseGame()
    {
        if (Time.timeScale == 1)
        {
            pauseActive = true;
            Time.timeScale = 0;
        }
        else
        {
            pauseActive = false;
            Time.timeScale = 1;
        }
    }
    public void Continue()
    {
        pauseActive = false;
        Time.timeScale = 1;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        TimeToComplete.instance.Continue();

        menuHolder.SetActive(false);
        buttonContinue.SetActive(false);
        buttonRestart.SetActive(false);
        buttonBackToMainMenu.SetActive(false);
    }
    public void Restart()
    {
        TimeToComplete.instance.ResetTimer();
        SceneManager.LoadScene(1);
    }
    public void BackToMainMenu()
    {
        SceneManager.LoadScene(0);
    }
}
