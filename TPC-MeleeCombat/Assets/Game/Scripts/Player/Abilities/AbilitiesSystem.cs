﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AbilitiesSystem : MonoBehaviour
{
    #region Singleton

    public static AbilitiesSystem instance;

    private void Awake()
    {
        instance = this;
    }

    #endregion

    public GameObject bg, bg2, bg3,
        aHb, eSb, mCb,
        sAb, dAb,
        nB, pB, cB;
    public Text skillPointsT;
    public bool inAB = false;
    public bool fA = false, sA = false, tA = false, nFA = false, aDA = false;
    void Start()
    {
        bg.SetActive(false);
        bg2.SetActive(false);
        bg2.SetActive(false);
        skillPointsT.gameObject.SetActive(false);
    }

    void Update()
    {
        ActiveAbilitiesBook();
    }
    private void ActiveAbilitiesBook()
    {
        skillPointsT.text = "Your skill points: " + PlayerController.instance.skillPoints.ToString();

        if (Input.GetKeyDown(KeyCode.P) && !inAB)
        {
            inAB = true;
            TimeToComplete.instance.Pause();

            if (inAB)
            {
                MenuInGameScript.instance.PauseGame();
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;

                bg.SetActive(true);
                bg2.SetActive(true);
                bg2.SetActive(true);
                skillPointsT.gameObject.SetActive(true);
            }
        }
        else if(Input.GetKeyDown(KeyCode.P) && inAB)
        {
            inAB = false;
            TimeToComplete.instance.Continue();

            if (!inAB)
            {
                Time.timeScale = 1f;
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;

                bg.SetActive(false);
                bg2.SetActive(false);
                bg2.SetActive(false);
                skillPointsT.gameObject.SetActive(false);
            }
        }

        if (!inAB && !MenuInGameScript.instance.pauseActive)
        {
            Time.timeScale = 1f;
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            TimeToComplete.instance.Continue();

            bg.SetActive(false);
            bg2.SetActive(false);
            bg2.SetActive(false);
            skillPointsT.gameObject.SetActive(false);
        }
    }

    #region AbilitiesButton
    public void AutoHealing()
    {
        if (PlayerController.instance.skillPoints > 0)
        {
            fA = true;
            PlayerController.instance.skillPoints -= 1;
            aHb.SetActive(false);
        }
    }
    public void EmpoweredSword()
    {
        if (PlayerController.instance.skillPoints > 0)
        {
            sA = true;
            PlayerController.instance.skillPoints -= 1;
            eSb.SetActive(false);
        }
    }
    public void MasterOfTheCombat()
    {
        if (PlayerController.instance.skillPoints > 0)
        {
            tA = true;
            PlayerController.instance.skillPoints -= 1;
            mCb.SetActive(false);
        }
    }
    public void SlowingArea()
    {
        if (PlayerController.instance.skillPoints > 0)
        {
            nFA = true;
            PlayerController.instance.skillPoints -= 1;
            sAb.SetActive(false);
        }
    }
    public void DamageArea()
    {
        if (PlayerController.instance.skillPoints > 0)
        {
            aDA = true;
            PlayerController.instance.skillPoints -= 1;
            dAb.SetActive(false);
        }
    }
    #endregion

    #region HUDButtons
    public void NextButton()
    {

    }
    public void PreviousButton()
    {

    }
    public void CloseButton()
    {
        bg.SetActive(false);
        bg2.SetActive(false);
        bg2.SetActive(false);
        skillPointsT.gameObject.SetActive(false);
        inAB = false;
        MenuInGameScript.instance.pauseActive = false;
    }
    #endregion
}
