﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllAbilities : MonoBehaviour
{
    #region Singleton

    public static AllAbilities instance;

    private void Awake()
    {
        instance = this;
    }

    #endregion

    #region FirstThreeAbilities
    public int heal, umbralHealth96, umbralHealth95, buffDamage, percentDamage;
    private int buff;
    public float timeToHeal, durationBuff, cooldown;
    private bool activeBuff = false, inCD = false;
    public bool activeMove;
    #endregion

    #region NewRoundOfAbilities
    #region SlowArea
    private int areaS;
    public float slow, areaDuration, cDArea, areaSlow;
    public bool activeArea;
    private bool activeCDA;
    #endregion

    #region DamageArea
    private int areaN;
    public int damageArea;
    public float areaDDuration, cDDArea;
    public bool activeDA;
    private bool activeCDDA;
    #endregion
    #endregion
    void Start()
    {
        #region SettingVariablesOfFirstRoundOfAbilities
        timeToHeal = 10f;
        durationBuff = 30f;
        cooldown = 45f;
        buffDamage = percentDamage;
        #endregion
    }

    void Update()
    {
        #region InvokeMethodsOfFirstAbilitiesRound
        umbralHealth96 = (int)(PlayerController.instance.maxHP * 0.96f);
        umbralHealth95 = (int)(PlayerController.instance.maxHP * 0.95f);

        buffDamage = percentDamage;
        percentDamage = (int)(PlayerController.instance.damage * 0.1f);

        ActiveSelfHeal();
        ActiveBuffDamage();
        Buff();
        ActiveMasterWeapon();
        #endregion

        #region InvokeMethodsOfSecondAbilitiesRound
        ActiveSlowingArea();
        ActivatingSlowingArea();
        ActiveCDSlowingArea();
        #endregion
    }
    #region RegionOfFirstAbilitiesMethods
    public void ActiveSelfHeal()
    {
        if (AbilitiesSystem.instance.fA)
        {
            timeToHeal -= Time.deltaTime;

            if (timeToHeal <= 0)
            {
                if (PlayerController.instance.currentHP >= PlayerController.instance.maxHP)
                {
                    PlayerController.instance.currentHP += 0;
                    timeToHeal = 10f;
                }
                if (PlayerController.instance.currentHP >= umbralHealth96)
                {
                    int a = PlayerController.instance.maxHP - PlayerController.instance.currentHP;
                    PlayerController.instance.currentHP += a;
                    timeToHeal = 10f;
                }
                if (PlayerController.instance.currentHP <= umbralHealth95)
                {
                    PlayerController.instance.currentHP += heal;
                    timeToHeal = 10f;
                }
            }
        }
    }
    public void ActiveBuffDamage()
    {
        if (AbilitiesSystem.instance.sA)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1) && activeBuff == false && buff == 0)
            {
                activeBuff = true;

                if (activeBuff)
                {
                    PlayerController.instance.damage += percentDamage;
                }

                buff = 1;
            }
        }
    }
    private void Buff()
    {
        if (activeBuff)
        {
            durationBuff -= Time.deltaTime;

            if (durationBuff <= 0)
            {
                durationBuff = 30f;
                activeBuff = false;
                PlayerController.instance.damage -= percentDamage;
                inCD = true;
            }
        }

        if (inCD)
        {
            cooldown -= Time.deltaTime;

            if (cooldown <= 0)
            {
                cooldown = 45f;
                buff = 0;
                inCD = false;
            }
        }
    }
    public void ActiveMasterWeapon()
    {
        if (AbilitiesSystem.instance.tA)
        {
            activeMove = true;
        }
    }
    #endregion

    #region RegionOfSecondAbilitiesMethods
    private void ActiveSlowingArea()
    {
        if (AbilitiesSystem.instance.nFA)
        {
            if (Input.GetKeyDown(KeyCode.Alpha2) && activeArea == false && areaS == 0)
            {
                activeArea = true;
                areaS = 1;
            }
        }
    }
    private void ActivatingSlowingArea()
    {
        if (activeArea)
        {
            areaDuration -= Time.deltaTime;

            if (areaDuration <= 0)
            {
                areaDuration = 10f;
                activeCDA = true;
                activeArea = false;
            }
        }
    }
    private void ActiveCDSlowingArea()
    {
        if (activeCDA)
        {
            cDArea -= Time.deltaTime;

            if (cDArea <= 0)
            {
                cDArea = 30f;
                areaS = 0;
                activeCDA = false;
            }
        }
    }


    private void ActiveDamageArea()
    {
        if (AbilitiesSystem.instance.aDA)
        {
            if (Input.GetKeyDown(KeyCode.Alpha3) && activeDA == false && areaN == 0)
            {
                activeDA = true;
                areaN = 1;
            }
        }
    }
    private void ActiveDurationArea()
    {

    }
    private void ActiveCDDamageArea()
    {

    }
    #endregion

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(this.transform.position, areaSlow);
    }
}
