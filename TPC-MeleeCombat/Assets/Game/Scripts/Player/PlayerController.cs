﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Animations;


[RequireComponent(typeof(CharacterController))]
public class PlayerController : MonoBehaviour
{
    #region Singleton

    public static PlayerController instance;

    private void Awake()
    {
        instance = this;
    }

    #endregion

    #region References
    public CharacterController player;
    public Animator anim;
    Rigidbody rb;

    public Transform cam;
    #endregion

    #region MovementsVariables
    public float moveSpeed;
    private float x, y;

    public float turnSmoothTime = 0.1f;
    private float turnSmoothVelocity;
    #endregion

    #region AnimationWeapon
    public float timeForAttacks;
    private int clicks;
    private bool canClick, isAttack;
    public BoxCollider weaponCollider;
    #endregion

    #region Stats
    public int currentHP, maxHP, damage;
    public Text ShowHP;
    public PlayerHealthBar healthBar;
    #endregion

    #region Experience&LevelSystem
    public PlayerExperience experienceBar;
    public int currentExp, maxExp, currentLevel, levelUp = 1, skillPoints;
    private bool newLevel = false;

    public GameObject levelUPText;
    public Text expText;
    public float timeToHideText;
    private bool activeText;
    #endregion


    void Start()
    {
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        player = GetComponent<CharacterController>();

        clicks = 0;
        canClick = true;

        skillPoints += 1;
        currentHP = maxHP;
        healthBar.SetMaxHealth(maxHP);
        experienceBar.SetMaxExperience(maxExp);

        levelUPText.SetActive(false);

        ShowHP.text = currentHP.ToString() + " / " + maxHP.ToString();
        expText.text = currentExp.ToString() + " / " + maxExp.ToString();
    }
    
    void Update()
    {
        #region Movements
        x = Input.GetAxis("Horizontal");
        y = Input.GetAxis("Vertical");
        Vector3 direction = new Vector3(x, 0f, y).normalized;

        if (direction.magnitude >= 0.1f)
        {
            float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + cam.eulerAngles.y;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);
            transform.rotation = Quaternion.Euler(0f, angle, 0f);

            Vector3 moveDir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;

            if (clicks == 0 || AllAbilities.instance.activeMove)
            {
                player.Move(moveDir.normalized * moveSpeed * Time.deltaTime);
            }
        }

        anim.SetFloat("VelX", x);
        anim.SetFloat("VelY", y);
        #endregion

        #region If Attack
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("Movement"))
        {
            weaponCollider.enabled = false;
        }

        if (Input.GetMouseButtonDown(0) && MenuInGameScript.instance.pauseActive == false)
        {
            BeginCombo();
        }

        SetAttackSpeed();
        #endregion

        #region Experience
        experienceBar.SetExperience(currentExp);
        expText.text = currentExp.ToString() + " / " + maxExp.ToString();
        ExperienceSystem();
        LevelSystem();
        #endregion

        #region Health&Destroyer
        healthBar.SetHealth(currentHP);
        ShowHP.text = currentHP.ToString() + " / " + maxHP.ToString();
        if (currentHP <= 0)
        {
            Destroy(this.gameObject);
        }
        #endregion
    }

    #region AnimationSection
    void BeginCombo()
    {
        if (canClick)
        {
            clicks++;
        }
        if (clicks == 1)
        {
            anim.SetInteger("attack", 1);
        }
    }
    public void CheckCombo()
    {
        canClick = false;

        if (anim.GetCurrentAnimatorStateInfo(0).IsName("FirstAttack") && clicks == 1)
        {
            anim.SetInteger("attack", 0); 
            canClick = true;
            clicks = 0;
        }
        else if (anim.GetCurrentAnimatorStateInfo(0).IsName("FirstAttack") && clicks >= 2)
        {
            anim.SetInteger("attack", 2);
            canClick = true;
        }
        else if (anim.GetCurrentAnimatorStateInfo(0).IsName("SecondAttack") && clicks == 2)
        {
            anim.SetInteger("attack", 0);
            canClick = true;
            clicks = 0;
        }
        else if (anim.GetCurrentAnimatorStateInfo(0).IsName("SecondAttack") && clicks >= 3)
        {
            anim.SetInteger("attack", 3);
            canClick = true;
        }
        else if (anim.GetCurrentAnimatorStateInfo(0).IsName("ThirdAttack"))
        {
            anim.SetInteger("attack", 0);
            canClick = true;
            clicks = 0;
        }
    }
    private void SetAttackSpeed()
    {
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("FirstAttack"))
        {
            anim.speed = 1.25f;
        }
        else if (anim.GetCurrentAnimatorStateInfo(0).IsName("SecondAttack"))
        {
            anim.speed = 1.25f;
        }
        else if (anim.GetCurrentAnimatorStateInfo(0).IsName("ThirdAttack"))
        {
            anim.speed = 1.25f;
        }
        else if (anim.GetCurrentAnimatorStateInfo(0).IsName("Movement"))
        {
            anim.speed = 1f;
        }
    }
    public void ActiveCollider()
    {
        weaponCollider.enabled = true;
    }
    public void DesactivateCollider()
    {
        weaponCollider.enabled = false;
    }
    #endregion

    #region SystemLevel
    private void ExperienceSystem()
    {
        if (currentExp >= maxExp)
        {
            activeText = true;
            newLevel = true;
            currentLevel += levelUp;
            skillPoints += 1;
            currentExp = 0;
        }

        if (activeText)
        {
            timeToHideText -= Time.deltaTime;
            levelUPText.SetActive(true);

            if (timeToHideText <= 0)
            {
                timeToHideText = 3f;
                levelUPText.SetActive(false);
                activeText = false;
            }
        }
    }
    private void LevelSystem()
    {
        if (newLevel)
        {
            maxHP += 20;
            damage += 5;
            newLevel = false;
        }
    }
    #endregion
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "EnemyWeapon")
        {
            currentHP -= CommonEnemy.instance.damage;
        }
    }
}
